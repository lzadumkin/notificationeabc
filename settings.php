<?php


/**
 * Notificationeabc enrolment plugin.
 *
 * This plugin notifies users when an event occurs on their enrolments (enrol, unenrol, update enrolment)
 *
 * @package    enrol_notificationeabc
 * @copyright  2016 e-ABC Learning
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    //--- general settings -----------------------------------------------------------------------------------
    
    $settings->add(new admin_setting_heading('enrol_notificationeabc_settings', '', get_string('pluginname_desc', 'enrol_notificationeabc'), ''));

    //notificacion de matriculacion
    // notification of enrol

    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeenrolalert', get_string('activeenrolalert', 'enrol_notificationeabc'), get_string('activeenrolalert_help', 'enrol_notificationeabc'), '1'));

    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeglobal', get_string('activeglobal', 'enrol_notificationeabc'), get_string('activeglobal_help', 'enrol_notificationeabc'), '0'));

    $settings->add(new admin_setting_confightmleditor('enrol_notificationeabc/enrolmessage', get_string('enrolmessage', 'enrol_notificationeabc'), get_string('enrolmessage_help', 'enrol_notificationeabc'), get_string('enrolmessagedefault', 'enrol_notificationeabc')));

   //notificacion de desmatriculacion
   // notification of unenrol
    
    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeunenrolalert', get_string('activeunenrolalert', 'enrol_notificationeabc'), get_string('activeunenrolalert_help', 'enrol_notificationeabc'), '1'));

    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeglobalunenrolalert', get_string('activeglobalunenrolalert', 'enrol_notificationeabc'), get_string('activeglobalunenrolalert_help', 'enrol_notificationeabc'), '0'));

    $settings->add(new admin_setting_confightmleditor('enrol_notificationeabc/unenrolmessage', get_string('unenrolmessage', 'enrol_notificationeabc'), get_string('unenrolmessage_help', 'enrol_notificationeabc'), get_string('unenrolmessagedefault', 'enrol_notificationeabc')));

	   //notificacion de actualizacion de matriculacion
       // notification of enrol updated
    
    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeenrolupdatedalert', get_string('activeenrolupdatedalert', 'enrol_notificationeabc'), get_string('activeenrolupdatedalert_help', 'enrol_notificationeabc'), '1'));

    $settings->add(new admin_setting_configcheckbox('enrol_notificationeabc/activeglobalenrolupdated', get_string('activeglobalenrolupdated', 'enrol_notificationeabc'), get_string('activeglobalenrolupdated_help', 'enrol_notificationeabc'), '0'));

    $settings->add(new admin_setting_confightmleditor('enrol_notificationeabc/updatedenrolmessage', get_string('updatedenrolmessage', 'enrol_notificationeabc'), get_string('updatedenrolmessage_help', 'enrol_notificationeabc'), get_string('updatedenrolmessagedefault', 'enrol_notificationeabc')));    

    // email settings

   $settings->add(new admin_setting_configtext('enrol_notificationeabc/subject', get_string('subject', 'enrol_notificationeabc'), '', get_string('subjectdefault','enrol_notificationeabc')));

   $settings->add(new admin_setting_configtext('enrol_notificationeabc/emailsender', get_string('emailsender', 'enrol_notificationeabc'), get_string('emailsender_help', 'enrol_notificationeabc'), ''));

   $settings->add(new admin_setting_configtext('enrol_notificationeabc/namesender', get_string('namesender', 'enrol_notificationeabc'), get_string('namesender_help', 'enrol_notificationeabc'), ''));
}
