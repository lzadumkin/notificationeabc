<?php

/**
 * Notificationeabc enrolment plugin.
 *
 * This plugin notifies users when an event occurs on their enrolments (enrol, unenrol, update enrolment)
 *
 * @package    enrol_notificationeabc
 * @copyright  2016 e-ABC Learning
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['enrolmessagedefault'] = 'You has been enroled in "{COURSENAME}" ({URL})';
$string['enrolmessage'] = 'Enrol message';
$string['messageprovider:notificationeabc_enrolment'] = 'Enrol notification messages';
$string['notificationeabc:manage'] = 'Manage notificationeabc';
$string['pluginname'] = 'Enrol Notification';
$string['pluginname_desc'] = 'Enrol notifications via mail';
$string['enrolmessage_help'] = 'Personalize the message that users will come to be enrolled. This field accepts the following markers which then will be replaced by the corresponding values ​​dynamically
<pre>
{COURSENAME} = course fullname
{USERNAME} = username
{FIRSTNAME} = firstname
{LASTNAME} = lastname
{URL} = course url 
</pre>';
$string['fecha_help'] = 'Place the period for which you want to perform the first virificación';
$string['fecha'] = 'Period for verification of users enrolled courses';
$string['active'] = 'Enable initial verification';
$string['active_help'] = 'When activated will be verified by the immediate execution of cron later, users who were enrolled for the period specified above';
$string['activeglobal'] = 'Activate global';
$string['activeglobal_help'] = 'Activate enrol notification for all site';
$string['emailsender'] = 'Email sender ';
$string['emailsender_help'] = 'By default set to take the email user support ';
$string['namesender'] = 'Name sender ';
$string['namesender_help'] = 'By default it takes the name set to the user support';
$string['status'] = 'Activate enrol notification';
//
$string['subject'] = 'Subject ';
$string['subjectdefault'] = 'Notification Enrollment';
//
$string['activeenrolalert'] = 'Activate enrol alert';
$string['activeenrolalert_help'] = 'Activate enrol alert';


//unenrol notifications
$string['activeunenrolalert'] = 'Activate unenrol notifications';
$string['activeunenrolalert_help'] = 'Activate unenrol alert';
$string['activeglobalunenrolalert'] = 'Activate global';
$string['activeglobalunenrolalert_help'] = 'Activate enrol notifications for all site';
$string['unenrolmessage'] = 'Unenrol message';
$string['unenrolmessage_help'] = 'Personalize the message that users will come to be unenrolled. This field accepts the following markers which then will be replaced by the corresponding values ​​dynamically
<pre>
{COURSENAME} = course fullname
{USERNAME} = username
{FIRSTNAME} = firstname
{LASTNAME} = lastname
{URL} = course url 
</pre>';
$string['unenrolmessagedefault'] = 'You has been unenrolled from "{COURSENAME}" ({URL})';


//Update enrol notifications
$string['activeenrolupdatedalert'] = 'Activate update enrol notifications';
$string['activeenrolupdatedalert_help'] = 'Activate update enrol notifications';
$string['activeglobalenrolupdated'] = 'Activate global';
$string['activeglobalenrolupdated_help'] = 'Activate enrol updated notifications for all site';
$string['updatedenrolmessage'] = 'Update enrollment message';
$string['updatedenrolmessage_help'] = 'Personalize the message that users will come to be updated. This field accepts the following markers which then will be replaced by the corresponding values ​​dynamically
<pre>
{COURSENAME} = course fullname
{USERNAME} = username
{FIRSTNAME} = firstname
{LASTNAME} = lastname
{URL} = course url
</pre>';
$string['updatedenrolmessagedefault'] = 'Your enrolment from "{COURSENAME}" has been updated ({URL})';