<?php

/**
 * Notificationeabc enrolment plugin.
 *
 * This plugin notifies users when an event occurs on their enrolments (enrol, unenrol, update enrolment)
 *
 * @package    enrol
 * @subpackage notificationeabc
 * @copyright  2016 e-ABC Learning
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/moodlelib.php');

class enrol_notificationeabc_plugin extends enrol_plugin
{

    private $log = '';    

    //funcion que envia las notificaciones a los usuarios

    /**
     * Function that sends notifications to users
     * @param stdClass $user instancia usuario // user instance
     * @param stdClass $course instancia curso // course instance
     * @param int $type aviso matriculacion, actualizacion o desmatriculacion // enrollment, update or unenrolment notice
     */
    public function sendmail(stdClass $user, stdClass $course, $type)
    {

        
        global $CFG, $DB, $COURSE;

        $course->url = $CFG->wwwroot.'/course/view.php?id='.$course->id;
        
        $enrol = $DB->get_record('enrol', array('enrol'=>'notificationeabc', 'courseid'=>$course->id, 'status'=>0));

        $activeglobalenrol = $this->get_config('activeglobal');

        $activeglobalunenrolalert = $this->get_config('activeglobalunenrolalert');

        $activeglobalenrolupdated = $this->get_config('activeglobalenrolupdated');

        // global messages
        $messageenrol = $this->get_config('enrolmessage');

        $plainmessageenrol = strip_tags($messageenrol);

        $messageunenrol = $this->get_config('unenrolmessage');

        $plainmessageunenrol = strip_tags($messageunenrol);

        $messageupdateenrol = $this->get_config('updatedenrolmessage');  

        $plainmessageupdateenrol = strip_tags($messageupdateenrol);      

        $subject = $this->get_config('subject');

        switch ((int)$type) {
            case 1:
            // enroll
                //si no se configuro un message personalizado se envia uno por defecto basico
                //if not configure a custom message you send global one
                if (!empty($enrol) && !empty($enrol->customtext1)) {
                    $texto = strip_tags($enrol->customtext1);
                    if (!empty($texto)) {
                        $message = $enrol->customtext1;
                    } else {
                        $message = get_string("enrolmessagedefault", "enrol_notificationeabc");
                    }
                    
                }else if (!empty($activeglobalenrol) && !empty($plainmessageenrol)) {
                	$message = $messageenrol;
                }else{
                    $message = get_string("enrolmessagedefault", "enrol_notificationeabc");
                }
                break;

            case 2:
            // unenroll
                if (!empty($enrol) && !empty($enrol->customtext2)) {
                    $texto = strip_tags($enrol->customtext2);
                    if (!empty($texto)) {
                        $message = $enrol->customtext2;
                    } else {
                        $message = get_string("unenrolmessagedefault", "enrol_notificationeabc");
                    }
                    
                }else if (!empty($activeglobalunenrolalert) && !empty($plainmessageunenrol)) {
                	$message = $messageunenrol;
                }else{
                    $message = get_string("unenrolmessagedefault", "enrol_notificationeabc");
                }
                break;

            case 3:
            //update enrollment
                if (!empty($enrol) && !empty($enrol->customtext3)) {
                    $texto = strip_tags($enrol->customtext3);
                    if (!empty($texto)) {
                        $message = $enrol->customtext3;
                    } else {
                        $message = get_string("updatedenrolmessagedefault", "enrol_notificationeabc");
                    }
                    
                }else if (!empty($activeglobalenrolupdated) && !empty($plainmessageupdateenrol)) {
                	$message = $messageupdateenrol;
                }else{
                    $message = get_string("updatedenrolmessagedefault", "enrol_notificationeabc");
                }
                break;
            
            default:
                break;
        }
        $message = $this->process_message($message, $user, $course);
        
        // email subject
        if (!empty($enrol) && !empty($enrol->customchar3)) {
            // from enrol instance
            $subject = $enrol->customchar3;
        }else if (!empty($subject)){
            // from global settings
            $subject = $this->get_config('subject');
        }
        else {
            // from default lang.pack
            $subject = get_string('subjectdefault');
        }

        //$soporte = core_user::get_support_user(); // default sender (support)
        $sender = get_admin(); // admin object

        if (!empty($enrol) && !empty($enrol->customchar1)) {
            // custom sender email
            $sender->email = $enrol->customchar1;
            $sender->maildisplay = true; // 
        }else{
            $sender->email =  $CFG->supportemail; //$soporte->email;
        }

        if (!empty($enrol) && !empty($enrol->customchar2)) {
            // custom sender Name
            $sender->firstname = $enrol->customchar2;
        }else{
            $sender->firstname = $CFG->supportname; //$soporte->firstname;
        }
        $sender->lastname = '';
        
        $mail = new \core\message\message(); //stdClass(); - used before M2.9
        $mail->courseid = $course->id; // M3.1 error!!!
        $mail->component = 'moodle';
        $mail->name = 'instantmessage';
        $mail->userfrom = $sender;
        $mail->userto = $user;//->id;
        $mail->subject = $subject;
        $mail->fullmessage = strip_tags($message);
        $mail->fullmessageformat = FORMAT_HTML;
        $mail->fullmessagehtml = $message;
        if (message_send($mail)) {
            $this->log.= "The user $user->username was notified about his-her enrollment in the course $course->fullname \n";
            //"Se notifico al usuario $user->username sobre su matriculación en el curso $course->fullname \n";
            return true;
        }else{
            $this->log.= "WARNING: Failed to notify the user $user->username on enrollment in the course $course->fullname \n"; 
            //"ATENCION: No se pudo notificar al usuario $user->username sobre su matriculación en el curso $course->fullname \n";
            return false;
        }
        
    } // end of function


    //procesa el mensaje para aceptar marcadores

    /**
     * Processes the message to accept markers
     * @param String $message el mensaje en bruto // the raw message
     * @param stdClass $user instancia usuario // user instance
     * @param stdClass $course instancia curso // course instance
     * @return String el mensaje procesado // the message processing
     */
    public function process_message($message, stdClass $user, stdClass $course){
        global $CFG;
        $m = $message;
        $url = new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id));
        $m = str_replace('{COURSENAME}', $course->fullname, $m);
        $m = str_replace('{USERNAME}', $user->username, $m);
        $m = str_replace('{FIRSTNAME}', $user->firstname, $m);
        $m = str_replace('{LASTNAME}', $user->lastname, $m);
        $m = str_replace('{URL}', $url, $m);
        return $m;
    }


    /**
     * Returns link to page which may be used to add new instance of enrolment plugin in course.
     * @param int $courseid
     * @return moodle_url page url
     */
    public function get_newinstance_link($courseid) {
        global $DB;
        $numenrol = $DB->count_records('enrol', array('courseid'=>$courseid, 'enrol'=>'notificationeabc'));
        $context = context_course::instance($courseid, MUST_EXIST);

        if (!has_capability('enrol/notificationeabc:manage', $context) or $numenrol >= 1) {
            return NULL;
        }
        return new moodle_url('/enrol/notificationeabc/edit.php', array('courseid'=>$courseid));
    }


    /**
     * Returns defaults for new instances.
     * @return array
     */
    public function get_instance_defaults() {

        $fields = array();
        $fields['customtext1']      = $this->get_config('enrolmessage');
        $fields['customtext2']      = $this->get_config('unenrolmessage');
        $fields['customtext3']      = $this->get_config('updatedenrolmessage');
        $fields['status']           = ENROL_INSTANCE_ENABLED;
        $fields['customint1']       = 0;
        $fields['customint2']       = 0;
        $fields['customint3']       = $this->get_config('activeenrolalert');
        $fields['customint4']       = $this->get_config('activeunenrolalert');
        $fields['customint5']       = $this->get_config('activeenrolupdatedalert');
        $fields['customchar1']      = $this->get_config('emailsender');
        $fields['customchar2']      = $this->get_config('namesender');
        $fields['customchar3']      = $this->get_config('subject');

        return $fields;
    }

    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'notificationeabc') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);

        $icons = array();

        if (has_capability('enrol/notificationeabc:manage', $context)) {
            $editlink = new moodle_url("/enrol/notificationeabc/edit.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('i/edit', get_string('edit'), 'core', array('class'=>'icon')));
        }

        return $icons;
    }


    public function get_info_icons(array $instances) {
        $icons = array();
        $icons[] = new pix_icon('email', get_string('pluginname', 'enrol_notificationeabc'), 'enrol_notificationeabc');
        return $icons;
    }

    /**
     * Is it possible to hide/show enrol instance via standard UI?
     * @param stdClass $instance
     * @return bool
     */
    public function can_hide_show_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/notificationeabc:manage', $context);
    }

    /**
     * Is it possible to delete enrol instance via standard UI?
     *
     * @param object $instance
     * @return bool
     */
    public function can_delete_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/notificationeabc:manage', $context);
    }

} // end of class

